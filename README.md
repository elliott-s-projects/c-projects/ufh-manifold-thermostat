# ufh-manifold-thermostat

This is a project to replace a broken underfloor heating manifold thermostat. This project take input from an analog temperature sensor embedded in the manifold and regulates the temperature into the manifold by controlling a motorised valve